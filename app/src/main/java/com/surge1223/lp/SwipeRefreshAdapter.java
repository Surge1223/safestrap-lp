package com.surge1223.lp;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

/**
 * Created by surge on 5/6/15.
 */
public class SwipeRefreshAdapter  extends SwipeRefreshLayout
{
    private HashSet m_listeners;
    ListView mListView;
    SwipeRefreshLayout mSwipeRefreshLayout;
    Adapter mAdapter;

    public SwipeRefreshAdapter(final Context context, final AttributeSet set) {
        super(context, set);
        this.m_listeners = new HashSet();
    }

    public void addScrollUpListener(final MultiROMSwipeRefreshLayout$ScrollUpListener multiROMSwipeRefreshLayout$ScrollUpListener) {
        this.m_listeners.add(multiROMSwipeRefreshLayout$ScrollUpListener);
    }

    @Override
    public boolean canChildScrollUp() {
        final Iterator<MultiROMSwipeRefreshLayout$ScrollUpListener> iterator = this.m_listeners.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().canChildScrollUp()) {
                return true;
            }
        }
        return false;
    }
}


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acivity_main);
        SwipeRefreshLayout mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_layout);
        mListView = findViewById(R.id.activity_main_list_view);
        mListView.setAdapter(new ArrayAdapter<String>(){
            String[] fakeTweets = getResources().getStringArray(R.array.fake_tweets);
            mAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, fakeTweets)
                    listView.setAdapter(mAdapter);
        });
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {

        listView.setAdapter();
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshContent();

            }

            // fake a network operation's delayed response
            // this is just for demonstration, not real code!
            private void refreshContent(){
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mAdapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, getNewTweets());
                        mListView.setAdapter(mAdapter);
                        mSwipeRefreshLayout.setRefreshing(false);
                    });
                }

                        // get new cat names.
                        // Normally this would be a call to a webservice using async task,
                        // or a database operation

                private List<String> getNewCatNames() {
                    List<String> newCatNames = new ArrayList<String>();
                    for (int i = 0; i < mCatNames.size(); i++) {
                        int randomCatNameIndex = new Random().nextInt(mCatNames.size() - 1);
                        newCatNames.add(mCatNames.get(randomCatNameIndex));
                    }
                    return newCatNames;
                }
}