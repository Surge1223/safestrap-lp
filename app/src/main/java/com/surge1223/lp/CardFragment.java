package com.surge1223.lp;

import android.annotation.TargetApi;


import android.app.AlertDialog;
import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.content.res.*;
import android.os.*;
import android.graphics.drawable.*;
import android.preference.*;
import android.widget.*;
import android.app.*;
import android.view.*;
import android.content.*;
/**
 * Created by surge on 5/6/15.
 */
public class CardFragment extends ActionBarActivity implements SwipeRefreshLayout$OnRefreshListener, MainActivityListener, StatusAsyncTask$StatusAsyncTaskListener
{
    private int m_curFragment;
    private DrawerLayout m_drawerLayout;
    private ListView m_drawerList;
    private CharSequence m_drawerTitle;
    private ActionBarDrawerToggle m_drawerToggle;
    private String[] m_fragmentTitles;
    private int m_fragmentViewsCreated;
    private MainFragment[] m_fragments;
    private MenuItem m_refreshItem;
    private MultiROMSwipeRefreshLayout m_srLayout;
    private CharSequence m_title;

    private void selectItem(final int curFragment) {
        if (curFragment < 0 || curFragment >= this.m_fragments.length) {
            Log.e("MROMMgr::MainActivity", "Invalid fragment index " + curFragment);
            return;
        }
        final FragmentTransaction beginTransaction = this.getFragmentManager().beginTransaction();
        if (this.m_curFragment != -1) {
            beginTransaction.hide((Fragment)this.m_fragments[this.m_curFragment]);
        }
        beginTransaction.show((Fragment)this.m_fragments[curFragment]);
        beginTransaction.commit();
        this.m_curFragment = curFragment;
        this.m_drawerList.setItemChecked(curFragment, true);
        this.setTitle(this.m_fragmentTitles[curFragment]);
        this.m_drawerLayout.closeDrawer((View)this.m_drawerList);
    }

    @TargetApi(20)
    private void showDeprecatedLAlert() {
        final SpannableString message = new SpannableString((CharSequence)this.getString(2131492908));
        Linkify.addLinks((Spannable) message, 15);
        final AlertDialog$Builder alertDialog$Builder = new AlertDialog$Builder((Context)this);
        alertDialog$Builder.setTitle(2131492909).setCancelable(false).setMessage((CharSequence)message).setNegativeButton(2131492907, (DialogInterface$OnClickListener)new MainActivity$4(this));
        final AlertDialog create = alertDialog$Builder.create();
        create.show();
        ((TextView)create.findViewById(16908299)).setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void addScrollUpListener(final MultiROMSwipeRefreshLayout$ScrollUpListener multiROMSwipeRefreshLayout$ScrollUpListener) {
        this.m_srLayout.addScrollUpListener(multiROMSwipeRefreshLayout$ScrollUpListener);
    }

    @Override
    public void onConfigurationChanged(final Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.m_drawerToggle != null) {
            this.m_drawerToggle.onConfigurationChanged(configuration);
        }
    }

    public void onCreate(final Bundle bundle) {
        super.onCreate(bundle);
        if (Build$VERSION.SDK_INT == 20) {
            this.showDeprecatedLAlert();
            return;
        }
        this.setContentView(2130903064);
        this.getWindow().setBackgroundDrawable((Drawable)null);
        Utils.installHttpCache((Context)this);
        PreferenceManager.setDefaultValues((Context)this, 2131034114, false);
        (this.m_srLayout = (MultiROMSwipeRefreshLayout)this.findViewById(2131361863)).setOnRefreshListener(this);
        this.m_curFragment = -1;
        this.m_fragmentTitles = this.getResources().getStringArray(2131099648);
        this.m_drawerLayout = (DrawerLayout)this.findViewById(2131361862);
        this.m_drawerList = (ListView)this.findViewById(2131361865);
        final String[] array = new String[2];
        for (int i = 0; i < array.length; ++i) {
            array[i] = MainFragment.getFragmentClass(i).getName();
        }
        this.m_fragments = new MainFragment[2];
        final FragmentManager fragmentManager = this.getFragmentManager();
        final FragmentTransaction beginTransaction = fragmentManager.beginTransaction();
        for (int j = 0; j < this.m_fragments.length; ++j) {
            this.m_fragments[j] = (MainFragment)fragmentManager.findFragmentByTag(array[j]);
            if (this.m_fragments[j] == null) {
                beginTransaction.add(2131361864, (Fragment)(this.m_fragments[j] = MainFragment.newFragment(j)), array[j]);
            }
            beginTransaction.hide((Fragment)this.m_fragments[j]);
        }
        beginTransaction.commit();
        this.m_drawerList.setAdapter((ListAdapter)new ArrayAdapter((Context)this, 2130903070, (Object[])this.m_fragmentTitles));
        this.m_drawerList.setOnItemClickListener((AdapterView$OnItemClickListener)new MainActivity$DrawerItemClickListener(this, null));
        this.m_drawerTitle = this.getText(2131492891);
        this.m_drawerToggle = new MainActivity$1(this, this, this.m_drawerLayout, 2131492915, 2131492914);
        this.m_drawerLayout.setDrawerListener(this.m_drawerToggle);
        final ActionBar supportActionBar = this.getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.setDisplayHomeAsUpEnabled(true);
            supportActionBar.setHomeButtonEnabled(true);
        }
        if (this.getIntent().hasExtra("show_rom_list") && this.getIntent().getBooleanExtra("show_rom_list", false)) {
            this.getIntent().removeExtra("show_rom_list");
            this.selectItem(1);
            return;
        }
        if (bundle != null) {
            this.selectItem(bundle.getInt("curFragment", 0));
            return;
        }
        this.selectItem(0);
    }

    public boolean onCreateOptionsMenu(final Menu menu) {
        this.getMenuInflater().inflate(2131623936, menu);
        this.m_refreshItem = menu.findItem(2131361917);
        if (!StatusAsyncTask.instance().isComplete()) {
            this.m_refreshItem.setEnabled(false);
        }
        return true;
    }

    @Override
    public void onFragmentViewCreated() {
        final int fragmentViewsCreated = 1 + this.m_fragmentViewsCreated;
        this.m_fragmentViewsCreated = fragmentViewsCreated;
        if (fragmentViewsCreated == this.m_fragments.length) {
            this.m_srLayout.postDelayed((Runnable)new MainActivity$3(this), 1L);
        }
    }

    @Override
    public void onFragmentViewDestroyed() {
        --this.m_fragmentViewsCreated;
    }

    @Override
    protected void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra("show_rom_list") && intent.getBooleanExtra("show_rom_list", false)) {
            this.selectItem(1);
        }
    }

    public boolean onOptionsItemSelected(final MenuItem menuItem) {
        if (this.m_drawerToggle.onOptionsItemSelected(menuItem)) {
            return true;
        }
        switch (menuItem.getItemId()) {
            default: {
                return false;
            }
            case 2131361917: {
                this.refresh();
                return true;
            }
            case 2131361919: {
                this.startActivity(new Intent((Context)this, (Class)SettingsActivity.class));
                return true;
            }
            case 2131361918: {
                new AlertDialog$Builder((Context)this).setTitle(2131492977).setCancelable(true).setNegativeButton(2131492898, (DialogInterface$OnClickListener)null).setItems(2131099649, (DialogInterface$OnClickListener)new MainActivity$2(this)).create().show();
                return true;
            }
        }
    }

    @Override
    protected void onPostCreate(final Bundle bundle) {
        super.onPostCreate(bundle);
        if (this.m_drawerToggle != null) {
            this.m_drawerToggle.syncState();
        }
    }

    @Override
    public void onRefresh() {
        this.refresh(false);
    }

    @Override
    protected void onSaveInstanceState(final Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("curFragment", this.m_curFragment);
    }

    @Override
    public void onStatusTaskFinished(final StatusAsyncTask$Result statusAsyncTask$Result) {
        for (int i = 0; i < this.m_fragments.length; ++i) {
            this.m_fragments[i].onStatusTaskFinished(statusAsyncTask$Result);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Utils.flushHttpCache();
    }

    @Override
    public void refresh() {
        this.refresh(true);
    }

    public void refresh(final boolean b) {
        StatusAsyncTask.destroy();
        UbuntuManifestAsyncTask.destroy();
        for (int i = 0; i < this.m_fragments.length; ++i) {
            this.m_fragments[i].refresh();
        }
        this.startRefresh(b);
    }

    @Override
    public void setRefreshComplete() {
        this.m_srLayout.setRefreshing(false);
        final MenuItem refreshItem = this.m_refreshItem;
        int i = 0;
        if (refreshItem != null) {
            this.m_refreshItem.setEnabled(true);
        }
        while (i < this.m_fragments.length) {
            this.m_fragments[i].setRefreshComplete();
            ++i;
        }
    }

    public void setTitle(final CharSequence title) {
        this.m_title = title;
        this.getSupportActionBar().setTitle(this.m_title);
    }

    public void startRefresh(final boolean b) {
        if (b) {
            this.m_srLayout.setRefreshing(true);
        }
        final MenuItem refreshItem = this.m_refreshItem;
        int i = 0;
        if (refreshItem != null) {
            this.m_refreshItem.setEnabled(false);
        }
        while (i < this.m_fragments.length) {
            this.m_fragments[i].startRefresh();
            ++i;
        }
        StatusAsyncTask.instance().setListener(this);
        StatusAsyncTask.instance().execute();
    }
}
