package com.hashcode.safestrap.ics;

import java.io.File;

import android.os.Handler;
import android.os.Message;

public class SELinuxDialogThread extends Thread {


	public Handler handler = null;
    public String packageCodePath = "";
    public File mAppRoot = null;
    public String LOGTAG = "";

	protected void pause(int milli) {
		try {
			Thread.sleep(milli);
		}
		catch(Exception ex) { }
	}

	protected void reply(int arg1, int arg2, String text) {
		Message msg = new Message();
		msg.arg1 = arg1;
		msg.arg2 = arg2;
		msg.obj = (Object)text;
		if (handler != null) { handler.sendMessage(msg); }
	}

	@Override
	public void run() {
		try {
			reply(1,0,"Trying to Change SELinux to Permissive...");
    		pause(2000);
    		AssetControl unzip = new AssetControl();
    		unzip.apkPath= packageCodePath;
    		unzip.mAppRoot = mAppRoot.toString();
    		unzip.LOGTAG = LOGTAG;
    		reply(1,0,"Unpacking Files...");
    		unzip.unzipAssets();
    		reply(1,50,"Checking SELinux enforcing address...");
        	String filesDir = mAppRoot.getAbsolutePath();
        	ExecuteAsRootBase.executecmd("chmod 755 " + filesDir + "/busybox");
        	ExecuteAsRootBase.executecmd("chmod 755 " + filesDir + "/*.sh");
    		reply(1,60,"Calculating physical to virtual offsets...");
        	ExecuteAsRootBase.executecmd("sh " + filesDir + "/recovery-install.sh " + filesDir);
			reply(1,75,"Checking SELinux status...");
			ExecuteAsRootBase.executecmd("sh " + filesDir + "/ss_enforcethis.sh " + filesDir);
			ExecuteAsRootBase.executecmd("sh " + filesDir + "/ss_enforcethis " + filesDir);
			Runtime.getRuntime().exec(new String[]{"su","-c","setenforce 0"});
    		reply(1,90,"Cleaning Up...");
        	
        	pause(1000);
        	reply(0,0,"SELinux status change  Complete.");
    	}
    	catch(Exception ex) {
    		reply(0,1,ex.getMessage());
    	}
    }

}
